/**
 * Nightw0rk
 * Datepicker with scroll
 */
(function ($) {
    $.DatePickerNight = function (element, options) {
        this.options = null;
        this.nowSelected = 0;
        this.Day = {begin: null, end: null};
        this.newSelectDay = null;
        this.onSelect = null;
        /**
         *
         * @param event
         */
        this.selectDay = function (event) {
            if ($(this).attr("rel") !== undefined) {
                var p = event.data.parent;
                var d = $(this).attr("rel").split(".");
                p.nowSelected = p.nowSelected == -1 ? 0 : p.nowSelected;
                switch (p.nowSelected) {

                    case 0:
                    {
                        p.newSelectDay = new Date(d[2], d[1] - 1, d[0]);
                        $("#" + p.options.containerBeginDay).text(d[0] + " " + p.options.monthSmallNames[parseInt(d[1] - 1)]);
                        $("#" + p.options.containerBeginDay).attr("rel", $(this).attr("rel"))
                        $("#" + p.options.containerEndDay).text("");
                        p.nowSelected = 1;
                        break;
                    }
                    case 1:
                    {
                        p.Day.end = new Date(d[2], d[1] - 1, d[0]);
                        p.Day.begin = new Date(p.newSelectDay.getTime());
                        p.newSelectDay = null;
                        $("#" + p.options.containerEndDay).text(d[0] + " " + p.options.monthSmallNames[parseInt(d[1] - 1)]);
                        $("#" + p.options.containerEndDay).attr("rel", $(this).attr("rel"))
                        $("#" + p.options.containerEndDay + "-value").val(d[2] + "-" + d[1] + "-" + d[0]);
                        var month = p.Day.begin.getMonth();
                        var mon = (parseInt(month) + 1) < 9 ? "0" + (parseInt(month) + 1) : (parseInt(month) + 1);
                        var dat = p.Day.begin.getDate() < 9 ? "0" + p.Day.begin.getDate() : p.Day.begin.getDate();
                        $("#" + p.options.containerBeginDay + "-value").val(p.Day.begin.getFullYear() + "-" + mon + "-" + dat);
                        p.nowSelected = -1;
                        p.markDay({data: {parent: p}});
                        if (typeof(p.onSelect) == "function") {
                            p.onSelect({begin: p.Day.begin, end: p.Day.end});
                        }
                        $("#" + p.options.containerId).hide();
                        break;
                    }
                }
            }
        };
        /**
         *
         * @param type [markIn||markOut]
         * @param date int[] 0=>day;1=>month;2=>year
         * @param object
         */
        this.setDate = function (object, type, date) {
            if (type == "markIn") {
                $("#" + this.options.containerBeginDay).text(date[0] + " " + this.options.monthSmallNames[parseInt(date[1] - 1)]);
                $("#markIn").parent().removeClass("selected");
                $("#markIn").remove();
                $("<span id='markIn'/>").addClass("subText").text(this.options.markIn).appendTo($(object));
                $(object).addClass("selected");
                $("#" + this.options.containerEndDay).text("");
            } else {
                $("#" + this.options.containerEndDay).text(date[0] + " " + this.options.monthSmallNames[parseInt(date[1] - 1)]);
                $("#markOut").parent().removeClass("selected");
                $("#markOut").remove();
                $("<span id='markOut'/>").addClass("subText").text(this.options.markOut).appendTo($(object));
                $(object).addClass("selected");
            }
        }
        /**
         *
         * @param event
         */
        this.preSelectDay = function (event) {
            if ($(this).attr("rel") !== undefined) {
                $(".selected").removeClass("selected");
                var p = event.data.parent;
                var d = $(this).attr("rel").split(".");
                switch (p.nowSelected) {
                    case 0:
                        p.setDate(this, "markIn", d);
                        break;
                    case -1:
                        p.setDate(this, "markIn", d);
                        break;
                    case 1:
                        p.setDate(this, "markOut", d);
                        p.markDay({data: {parent: p, curSelect: d}});
                        break;
                }

            }
        }
        /**
         *
         * @param event
         */
        this.markDay = function (event) {
            if (typeof(event.data) == "object") {
                var p = event.data.parent;
                if (typeof(event.relatedTarget) == "object") {
                    if (event.relatedTarget.className.split(" ")[0] == p.options.containerDayId) {
                        return;
                    }
                    if (event.relatedTarget.id == p.options.containerWeekId) {
                        return;
                    }
                }
                $(".selected").removeClass("selected");
                var db = p.newSelectDay ? p.newSelectDay : p.Day.begin;
                var month = db.getMonth();
                var mon = (parseInt(month) + 1) < 9 ? "0" + (parseInt(month) + 1) : (parseInt(month) + 1);
                var begin = $("." + p.options.containerDayId + "[rel='" + db.getDate() + "." + mon + "." + db.getFullYear() + "']");
                $("#" + p.options.containerBeginDay).text(db.getDate() + " " + p.options.monthSmallNames[month]);
                begin.addClass("selected");
                var day = p.Day.end;
                if (typeof(event.data.curSelect) == "object")
                    day = new Date(event.data.curSelect[2], event.data.curSelect[1] - 1, event.data.curSelect[0]);
                var countDay = (day - db) / 1000 / 60 / 60 / 24;
                var selector = "";
                var dateDay = new Date(db.getTime());
                for (var i = 1; i <= countDay; i++) {
                    dateDay.setDate(dateDay.getDate() + 1);
                    month = dateDay.getMonth();
                    mon = (parseInt(month) + 1) < 9 ? "0" + (parseInt(month) + 1) : (parseInt(month) + 1);
                    selector = "." + p.options.containerDayId + "[rel='" + dateDay.getDate() + "." + mon + "." + dateDay.getUTCFullYear() + "']";
                    var day_ = $(selector);
                    day_.addClass("selected");
                }
                $("#markIn").remove();
                $("<span id='markIn'/>").addClass("subText").text(p.options.markIn).appendTo(begin);
                month = day.getMonth();
                mon = (parseInt(month) + 1) < 9 ? "0" + (parseInt(month) + 1) : (parseInt(month) + 1);
                var end = $("." + p.options.containerDayId + "[rel='" + day.getDate() + "." + mon + "." + day.getFullYear() + "']");
                end.addClass("selected");
                $("#" + p.options.containerEndDay).text(day.getDate() + " " + p.options.monthSmallNames[month]);
                $("#markOut").remove();
                $("<span id='markOut'/>").addClass("subText").text(p.options.markOut).appendTo(end);
            }
        }

        /**
         *
         */
        this.setEvents = function () {
            $("#" + this.options.containerId).tinyscrollbar();
            $(document).bind("mousedown", {opt: this.options}, DefaultOnBlur);
            $("." + this.options.containerDayId).bind("click", {parent: this}, this.selectDay);
            $("." + this.options.containerDayId).bind("mouseover", {parent: this}, this.preSelectDay);
            $("." + this.options.containerMonthId).bind("mouseout", {parent: this}, this.markDay);
            this.onSelect = this.options.onSelect;
        };
        /**
         *
         * @param element
         * @param options
         */
        this.init = function (element, options) {
            var d;
            options = $.extend({}, $.DatePickerNight.defaultOptions, options);
            generateCalendar(options);
            element.bind("click", {opt: options}, options.onClick);
            if (!options.beginDay) {
                d = new Date();
                this.Day.begin = d;
                $("#" + options.containerBeginDay).text(d.getDate() + " " + options.monthSmallNames[d.getMonth()]);
            } else
                this.Day.begin = new Date(options.beginDay);
            if (!options.endDay) {
                d = new Date();
                d = new Date(d.getFullYear(), d.getMonth(), d.getDate() + 1);
                this.Day.end = d;
                $("#" + options.containerEndDay).text(d.getDate() + " " + options.monthSmallNames[d.getMonth()]);
            } else
                this.Day.end = new Date(options.endDay);
            this.nowSelected = 0;
            options.element = element;
            this.options = options;
            this.setEvents();
            this.markDay({data: {parent: this}});
        };

        this.init(element, options);

    };
    /**
     * Default click function
     * @param event
     * @constructor
     */
    function DefaultOnClick(event) {
        var popup = $("#" + event.data.opt.containerId);
        if (event.data.opt.element.css("z-index") > 0) {
            if (!popup.is(":visible")) {
                var target = $(event.currentTarget);
                var position = {
                    leftX: target.offset().left,
                    topX: target.offset().top + target.height()
                };
                popup.css("top", position.topX + 10 + "px").css("left", position.leftX + "px").show();
                $("#" + event.data.opt.containerId).tinyscrollbar_update();
            }
        }
    }

    /**
     *
     */
    function DefaultOnBlur(event) {
        var target = $(event.target);
        var parent = $("#" + event.data.opt.containerId);
        if (parent.has(target).length == 0) {
            parent.hide();
        }
    }

    /**
     *
     * @param holder
     * @param options
     */
    function createWeekContainer(holder, options) {
        var weekContainer = document.createElement("div");
        weekContainer.setAttribute("id", options.containerWeekId);
        holder.appendChild(weekContainer);
        return weekContainer;
    }

    /**
     * Generate month
     * @param day
     * @param month
     * @param Year
     * @param container
     * @param options
     */
    function generateMonth(day, month, Year, container, options) {
        var containerDay;
        var getLastDay = new Date(Year, month + 1, 0).getDate();
        var holder = document.createElement("div");
        holder.className = options.containerMonthId + " ";
        holder.className += month % 2 ? options.containerMonthClassEven : options.containerMonthClassOdd;
        var week = createWeekContainer(holder, options);
        var dd = new Date(Year, month, day).getDay();
        dd = dd == 0 ? 7 : dd;
        if (dd > 1) {
            var currDate = new Date();
            if (currDate.getMonth() != month)
                week.className += options.containerWeekClassNotFull;
            for (var i = 1; i < dd; i++) {
                containerDay = document.createElement("div");
                containerDay.setAttribute("class", options.containerDayId);
                containerDay.className += " " + options.containerEmptyDay;
                containerDay.innerHTML = "&nbsp;";
                week.appendChild(containerDay);

            }
        }
        for (var d = day; d <= getLastDay; d++) {
            var DayOfWeek = new Date(Year, month, d).getDay();
            containerDay = document.createElement("div");
            var isWeekClass = DayOfWeek == 0 || DayOfWeek == 6 ? "weekend" : "";
            containerDay.setAttribute("class", options.containerDayId);
            containerDay.className += " " + isWeekClass;
            var mon = (parseInt(month) + 1) < 9 ? "0" + (parseInt(month) + 1) : (parseInt(month) + 1);
            containerDay.setAttribute("rel", d + "." + mon + "." + Year);

            containerDay.innerHTML = "<span class='day-text'>" + d + "</span>";
            week.appendChild(containerDay);
            if (DayOfWeek % 7 == 0) {
                week = createWeekContainer(holder, options);
            }
        }
        container.appendChild(holder);
        return holder;
    }

    /**
     *
     * @param options
     */
    function fillCalendarHeader(options) {
        var element = document.createElement("div");
        element.id = options.containerClassCalendarHeader;
        for (var i = 0; i <= 6; i++) {
            var spanClass = i == 5 || i == 6 ? "class='weekend'" : "";
            element.innerHTML += "<span " + spanClass + ">" + options.namesDay[i] + "</span>";
        }
        return element;
    }

    function createMonthWrap(currentDate, options, container) {
        var monthContainer = document.createElement("div");
        generateMonth(
            currentDate.getDate(),
            currentDate.getMonth(),
            currentDate.getFullYear(),
            monthContainer, options);
        var title = document.createElement("div");
        title.className = "month-title";
        title.innerHTML = options.monthNames[currentDate.getMonth()];
        monthContainer.appendChild(title);
        monthContainer.className += options.monthWrap;
        container.appendChild(monthContainer);
        return {monthContainer: monthContainer, title: title};
    }

    /**
     * Generate Calendar
     * @param options
     */
    function generateCalendar(options) {
        var container = document.createElement("div");
        container.setAttribute("id", options.containerId);
        container.tabIndex = -1;
        var wrapContainer = document.createElement("div");
        wrapContainer.setAttribute("id", options.containerCalendarWrap);
        wrapContainer.className += "viewport";
        var overview = document.createElement("div");
        overview.className = "overview";
        wrapContainer.appendChild(overview);
        container.appendChild(fillCalendarHeader(options));
        var currentDate = new Date();
        createMonthWrap(currentDate, options, overview);
        for (var i = 1; i < options.rangeMonth; i++) {
            var ce = new Date(currentDate.getFullYear() + ((currentDate.getMonth() + i) > 11 ? 1 : 0), (currentDate.getMonth() + i) % 12, 1);
            createMonthWrap(ce, options, overview);
        }
        container.appendChild(wrapContainer);
        var scroll = document.createElement("div");
        scroll.setAttribute("id", options.containerCalendarScroll);
        scroll.className = "scrollbar";
        var track = document.createElement("div");
        track.className = "track";
        var scrollSlider = document.createElement("div");
        scrollSlider.setAttribute("id", options.containerCalendarScroll + "-slider");
        scrollSlider.className = "thumb";
        track.appendChild(scrollSlider);
        scroll.appendChild(track);
        container.appendChild(scroll);
        document.body.appendChild(container);
    }

    /**
     *
     * @param options
     * @returns {*}
     * @constructor
     */
    $.fn.DatePickerNight = function (options) {
        return this.each(function () {
            (new $.DatePickerNight($(this), options));
        })
    };
    /**
     *
     * @type {{showNumberMonth: number, rangeMonth: number, stepScroll: number, debug: boolean, onClick: *, containerId: string, containerMonthId: string, containerMonthClassEven: string, containerMonthClassOdd: string, containerWeekId: string, containerDayId: string, containerEmptyDay: string, containerWeekClassNotFull: string, containerClassCalendarHeader: string, monthNames: Array, namesDay: Array}}
     */
    $.DatePickerNight.defaultOptions =
    {
        showNumberMonth: 3,
        rangeMonth: 12,
        stepScroll: 50,
        currentScroll: 0,
        debug: false,
        markIn: "Заезд",
        markOut: "Выезд",
        onClick: DefaultOnClick,
        element: null,
        containerId: "datepicker-night-div",
        containerBeginDay: "datepicker-night-div-span-beginday",
        containerEndDay: "datepicker-night-div-span-endday",
        containerCalendarWrap: "datepicker-night-div-wrap",
        containerCalendarScroll: "datepicker-night-div-scroll",
        containerMonthId: "datepicker-night-div-month",
        containerMonthClassEven: "even",
        containerMonthClassOdd: "odd",
        containerWeekId: "datepicker-night-div-week",
        containerDayId: "datepicker-night-div-day",
        containerEmptyDay: "empty-day",
        containerWeekClassNotFull: "not-full",
        containerClassCalendarHeader: "datepicker-night-div-header",
        monthWrap: "datepicker-night-div-month-wrap",
        monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
        monthSmallNames: [ "янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек" ],
        namesDay: ["Пн.", "Вт.", "Ср.", "Чт.", "Пт.", "Сб.", "Вс."],
        beginDay: null,
        endDay: null,
        onSelect: null
    }
})(jQuery);
